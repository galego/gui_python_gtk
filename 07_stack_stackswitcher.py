from gi import require_version as version
version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Abas e conteúdo")
        self.set_border_width(14)
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.add(box)

        # Stack
        main_area = Gtk.Stack()
        main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        main_area.set_transition_duration(800)

        # Check
        check_button = Gtk.CheckButton("Do not fn check me")
        main_area.add_titled(check_button, "Check Name", "Check Box")

        # Label
        label = Gtk.Label()
        label.set_markup("<big>huge text</big>")
        main_area.add_titled(label, "Label name", "big label")

        # Switcher
        switcher = Gtk.StackSwitcher()
        switcher.set_stack(main_area)
        box.pack_start(switcher, True, True, 0)
        box.pack_start(main_area, True, True, 0)



window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
