import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Escolha de arquivo")
        self.set_border_width(30)
        layout = Gtk.Box(spacing=8)
        self.add(layout)

        button = Gtk.Button("Choose File...")
        button.connect("clicked", self.on_file_clicked)
        layout.add(button)

    # User clicked the choose file button
    def on_file_clicked(self, widget):
        dialog = Gtk.FileChooserDialog("Select a file", self, Gtk.FileChooserAction.OPEN,
                                       ("Cancel", Gtk.ResponseType.CANCEL,
                                        "Ok", Gtk.ResponseType.OK))
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            print(f"File selected: {dialog.get_filename()}")
        elif response == Gtk.ResponseType.CANCEL:
            print("Operation Aborted")

        dialog.destroy()


win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()