from gi import require_version as version
version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Text Styling")
        self.set_border_width(15)
        self.set_default_size(500, 400)

        # Boxes
        hbox = Gtk.Box(spacing=20)
        hbox.set_homogeneous(False)
        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        vbox_right.set_homogeneous(False)

        # Pack the two columns
        hbox.pack_start(vbox_left, True, True, 0)
        hbox.pack_start(vbox_right, True, True, 0)

        # Normal
        label = Gtk.Label("This is a plain label")
        vbox_left.pack_start(label, True, True, 0)

        # Left Aligned
        label = Gtk.Label()
        label.set_text("This is left aligned\nAnother line")
        label.set_justify(Gtk.Justification.LEFT)
        vbox_left.pack_start(label, True, True, 0)

        # Right Aligned
        label = Gtk.Label("Qualquer coisa\nOutra coisa")
        label.set_justify(Gtk.Justification.RIGHT)
        vbox_left.pack_start(label, True, True, 0)

        # Line wrap
        label = Gtk.Label("Texto a ser inserido com quebra (wrap) se chegar ao canto da janela. Mais texto e outra frase: 'Guardo o coração bem longe do olhar para não me iludir com quem se aproximar. As palavras podem ser, até sem parecer, armadilhas.'")
        label.set_line_wrap(True)
        vbox_right.pack_start(label, True, True, 0)

        # Fill (newspaper) == Justified
        label = Gtk.Label("Muito texto escrito neste espaço para servir como exemplo do que vai acontecer com essa nova configuração")
        label.set_line_wrap(True)
        label.set_justify(Gtk.Justification.FILL)
        vbox_right.pack_start(label, True, True, 0)

        # Markup
        label = Gtk.Label()
        label.set_markup("<small>Small Text</small>\n\
                          <big>big</big>\n\
                          <b>bold</b>\n\
                          <i>itálico</i>\
                          <a href='https://python.org' title='secret link'>wtfigo</a>")
        label.set_line_wrap(True)
        vbox_right.pack_start(label, True, True, 0)

        self.add(hbox)

window = MainWindow()
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
