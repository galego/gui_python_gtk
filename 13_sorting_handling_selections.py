import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


# List of tuples (the model, a.k.a. the data that will be displayed by the treeview)
people = [("Charlie Chaplin", 67, "Actor"),
          ("Ayrton Senna", 36, "Formula 1 pilot"),
          ("Emma Anderson", 25, "Nurse"),
          ("Jennifer Nayara", 26, "Actress"),
          ("Anderson Azzevedo", 30, "Programmer")]


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="People Finder")
        self.set_border_width(8)
        layout = Gtk.Box()
        self.add(layout)

        people_list_store = Gtk.ListStore(str, int, str)
        for item in people:
            people_list_store.append(list(item))

        people_tree_view = Gtk.TreeView(people_list_store)

        for i, col_title in enumerate(["Name", "Age", "Profession"]):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(col_title, renderer, text=i)
            column.set_sort_column_id(i)
            people_tree_view.append_column(column)

        # Handle selection
        selected_row = people_tree_view.get_selection()
        selected_row.connect("changed", self.item_selected)

        layout.pack_start(people_tree_view, True, True, 0)

    # User selected row
    def item_selected(self, selection):
        model, row = selection.get_selected()
        if row is not None:
            print(f"Name: {model[row][0]}")
            print(f"Age: {model[row][1]}")
            print(f"Profession: {model[row][2]}")
            print()



win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()