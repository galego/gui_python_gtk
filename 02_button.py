from gi import require_version as version
version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Uma janela para outra dimensão")

        # Button
        self.button = Gtk.Button(label="clique e desapareça")
        self.button.connect("clicked", self.button_clicked)
        self.add(self.button)

    # User clicks button
    def button_clicked(self, widget):
        print("I got clicked!")

window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()

Gtk.main()