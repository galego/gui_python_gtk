import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


# List of tuples (the model, a.k.a. the data that will be displayed by the treeview)
people = [("Charlie Chaplin", 67, "Actor"),
          ("Ayrton Senna", 36, "Formula 1 pilot"),
          ("Emma Anderson", 25, "Nurse"),
          ("Jennifer Nayara", 26, "Actress"),
          ("Anderson Azzevedo", 30, "Programmer")]


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="People Finder")
        self.set_border_width(8)
        layout = Gtk.Box()
        self.add(layout)

        # Convert data do ListStore (lists that TreeView can display)
        people_list_store = Gtk.ListStore(str, int, str)
        for item in people:
            people_list_store.append(list(item))

        # TreeView is the item that is displayed
        people_tree_view = Gtk.TreeView(people_list_store)

        for i, col_title in enumerate(["Name", "Age", "Profession"]):
            # Render means how to draw the data
            renderer = Gtk.CellRendererText()

            # Create columns (text is column number)
            column = Gtk.TreeViewColumn(col_title, renderer, text=i)

            # Add column to TreeView
            people_tree_view.append_column(column)

        # Add TreeView to the layout
        layout.pack_start(people_tree_view, True, True, 0)


win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()