import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Notebook")
        self.set_border_width(15)
        self.notebook = Gtk.Notebook()
        self.add(self.notebook)

        # page 1
        self.page1 = Gtk.Box()
        self.page1.set_border_width(10)
        self.page1.add(Gtk.Label("Can't see it my way"))
        self.notebook.append_page(self.page1, Gtk.Label("First tab"))

        # page 2
        self.page2 = Gtk.Box()
        self.page2.set_border_width(10)
        self.page2.add(Gtk.Label("Hello, is there anybody in there?"))
        icon = Gtk.Image.new_from_icon_name("gnome-dev-cdrom-audio",
                                            Gtk.IconSize.MENU)
        self.notebook.append_page(self.page2, icon)



win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
