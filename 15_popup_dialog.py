import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Pop-up Dialog")
        self.set_border_width(10)
        self.set_default_size(200, 100)

        button = Gtk.Button("Open Pop up")
        button.connect("clicked", self.button_clicked)
        self.add(button)

    def button_clicked(self, widget):
        dialog = PopUp(self)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            print("Ok, continuando...")
        elif response == Gtk.ResponseType.CANCEL:
            print("Abort mission")

        dialog.destroy()


class PopUp(Gtk.Dialog):

    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "PopUp Title", parent, Gtk.DialogFlags.MODAL,
                            ("Cancel", Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_border_width(30)
        self.set_default_size(200, 100)

        area = self.get_content_area()
        area.add(Gtk.Label("And the world is going on"))
        self.show_all()



win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()