from gi import require_version as version
version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Hello World!")

        self.box = Gtk.Box(spacing=10)
        self.add(self.box)

        self.bacon_button = Gtk.Button(label="Bacon is Healthy")
        self.bacon_button.connect("clicked", self.button_clicked) #, "bacon")
        self.box.pack_start(self.bacon_button, True, True, 0)

        self.tomato_button = Gtk.Button(label="Tomato is ok")
        self.tomato_button.connect("clicked", self.button_clicked) #, "tomato")
        self.box.pack_end(self.tomato_button, True, True, 0)

        self.foo_button = Gtk.Button(label="FOO")
        self.foo_button.connect("clicked", self.button_clicked) #, "foo")
        self.box.pack_start(self.foo_button, True, True, 0)

    # Há várias formas de imprimir uma mensagem adequada.
    # Só substituir o código atual adicionando as partes comentadas.
    def button_clicked(self, widget): #, msg):
        # if msg == "bacon":
        #     print("Bacon is good")
        # elif msg == "tomato":
        #     print("Really, tomato can save you")
        # elif msg == "foo":
        #     print("Noboy knows what's happening")
        msg = widget.get_label().lower()
        if "bacon" in msg:
            print("Bacon is good")
        elif "tomato" in msg:
            print("Tomato save lifes")
        elif "foo" in msg:
            print("Whaaaaaaaat???")



window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()