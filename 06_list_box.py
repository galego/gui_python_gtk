from gi import require_version as version
version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        super(Gtk.Window, self).__init__(title="Pedidos Burgeria")
        self.set_border_width(10)

        # List Box
        listBox = Gtk.ListBox()
        listBox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.add(listBox)

        # checkbox
        row_1 = Gtk.ListBoxRow()
        box_1 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=100)
        row_1.add(box_1)
        label = Gtk.Label("Check for the good")
        check = Gtk.CheckButton()
        box_1.pack_start(label, True, True, 0)
        box_1.pack_start(check, True, True, 0)
        listBox.add(row_1)

        # toggle switch
        row_2 = Gtk.ListBoxRow()
        box_2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=100)
        row_2.add(box_2)
        label1 = Gtk.Label("Ligar/Desligar")
        switch = Gtk.Switch()
        box_2.pack_start(label1, True, True, 0)
        box_2.pack_start(switch, True, True, 0)
        listBox.add(row_2)



window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
