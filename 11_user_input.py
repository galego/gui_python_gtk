from gi import require_version as version
version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="User Input")
        self.set_size_request(200, 100)
        self.set_border_width(15)

        # Layout
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=8)
        self.add(vbox)

        # Username
        label = Gtk.Label("Name:")
        label.set_halign(Gtk.Align.START)
        self.username = Gtk.Entry()
        vbox.pack_start(label, True, True, 0)
        vbox.pack_start(self.username, True, True, 0)

        # Password
        label = Gtk.Label(label="Password:")
        label.set_halign(Gtk.Align.START)
        self.password = Gtk.Entry()
        self.password.set_visibility(False)
        vbox.pack_start(label, True, True, 0)
        vbox.pack_start(self.password, True, True, 0)

        # Sign in
        self.button = Gtk.Button(label="Sign In")
        self.button.connect("clicked", self.sign_in)
        vbox.pack_start(self.button, True, True, 0)

    def sign_in(self, widget):
        print(self.username.get_text(), self.password.get_text())


win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()